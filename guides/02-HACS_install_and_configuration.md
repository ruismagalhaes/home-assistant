# Introduction

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

Pre-requisite:

- Installation of HASSIO on a virtual machine ([guide here](https://gitlab.com/ruismagalhaes/home-assistant/-/blob/master/guides/01-HASSIO_virtualbox_install.md))
- [Terminal addon installed](https://github.com/home-assistant/hassio-addons/tree/master/ssh)

## What HACS can do:

In short:

Help you discover new custom elements.
Help you install (download) new custom elements.
Help you keep track of your custom elements.

# Installation

1. Step 1 - Download HACS newest release from [here](https://github.com/custom-components/hacs/releases/latest)

2. Step 2 - Extract the content

3. Step 3 - Create custom_components folder

4. Step 4 - Copy the extracted folder to /config/custom_components/hacs

example:

```bash
$ scp -r <user>@<server_internal_ip>:/path/to/remote/source /config/custom_components/hacs
```

5. Step 5 - Restart HASSIO

# Configuration

1. Step 1 - Generate a new personal access token on github - https://github.com/settings/tokens/new. No need to check any box when creating the new token

2. Step 2 - Copy the token

3. Step 3 - Go to the "integration" tab and install HACS

4. Step 4 - Paste the token


# Sources

1. https://hacs.xyz/docs/installation/manual

2. https://hacs.xyz/docs/configuration/pat
