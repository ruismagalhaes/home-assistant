# HOME ASSISTANT SET UP

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

This Tuturial is to explain how you can install Home assistant (hassio - it has the operating system + the home-assistant core).

**NOTE1:** This way of installation was choosed because the docker way does not have the addons store available. 
On top of that, the supervised way (linux + docker, I guess) it's not supported anymore. 
So, because of that, and adding the opportunity to learn about a virtualization environment software, I choosed installing the image (OS + core).

# Table of contents

[[_TOC_]] 

## VirtualBox steps

1. Create the VM with a **name**, a **OS type** and the folder where VirtualBox will create the virtual machine file (**basefolder**). The "**--register**" flag will perform the registering the VM with VirtualBox:

```bash
sudo vboxmanage createvm --name "home-assistant-hassio" --ostype "Linux_64" --register --basefolder /home/ruimag/desktop/home-assistant/vm-folder/
```

2. Download the virtual hard disk file from Home Assistant website (https://www.home-assistant.io/hassio/installation/):

```bash
cd /home/ruimag/desktop/home-assistant/vm-folder/home-assistant-hassio
wget https://github.com/home-assistant/operating-system/releases/download/4.11/hassos_ova-4.11.vdi.gz
gunzip hassos_ova-4.11.vdi.gz
mv hassos_ova-4.11.vdi Linux_64_hd.vdi
``` 

3. Set memory RAM to 4GB:

```bash
sudo vboxmanage modifyvm "home-assistant-hassio"  --memory 4096
```


4. Set the hard drive with 200GB and use the virtual Hard Disk downloaded before:

```bash
sudo vboxmanage modifyhd /home/ruimag/desktop/home-assistant/vm-folder/home-assistant-hassio/Linux_64_hd.vdi --resize 200000
sudo vboxmanage storagectl "home-assistant-hassio" --name "SATA Home assistant" --add sata --controller IntelAhci
sudo vboxmanage storageattach "home-assistant-hassio" --storagectl "SATA Home assistant" --port 0 --device 0 --type hdd --medium  /home/ruimag/desktop/home-assistant/vm-folder/home-assistant-hassio/Linux_64_hd.vdi
```

5. Enable "**EFI**" and set "**network**", "**adapter 1**" to "**bridged**" and select the "**enp37s0**" interface. On top of that, enable the use of hardware virtualization extensions:

```bash
sudo vboxmanage modifyvm "home-assistant-hassio" --firmware efi
sudo vboxmanage modifyvm "home-assistant-hassio" --nic1 bridged --bridgeadapter1 enp37s0
sudo vboxmanage modifyvm "home-assistant-hassio" --hwvirtex on
```

**NOTE2:** In order to get the active network interface run the following:

```bash
ifconfig | awk -F: '/^en/ { print $1 }'
```

6. Start the VM:

```bash
sudo vboxmanage startvm "home-assistant-hassio" --type headless
```

After waiting a while, you should be able to visit [ this page ](http://homeassistant.local:8123/). If your router does not support mDNS just go check the IP address that **home-assistant** got and access it through the browser, on port 8123.
The [ link ](http://homeassistant.local:8123/) should take you to the following screen, to create your account:


<div align="center">
<img src="./images/home-assistant-first-login.png"  width="400" height="400">
</div>

7. To stop the machine:

```bash
vboxmanage controlvm "home-assistant-hassio" poweroff --type headless
```

## Creating a systemd service for home-assistant

1. Let’s create a file called **/etc/systemd/system/home-assistant.service** :

```
[Unit]
Description=home-assistant virtualbox Vm
After=network.target vboxdrv.service
Before=runlevel2.target shutdown.target

[Service]
User=root
Group=root
Type=forking
Restart=always
RestartSec=5
TimeoutSec=5min
IgnoreSIGPIPE=no
KillMode=process
GuessMainPID=no
RemainAfterExit=yes

ExecStart=/usr/bin/VBoxManage startvm home-assistant-hassio --type headless
ExecStop=/usr/bin/VBoxManage controlvm home-assistant-hassio poweroff --type headless

SuccessExitStatus=1

[Install]
WantedBy=multi-user.target
```

2. Reload you daemon and enable your service to start on boot:

```bash
sudo systemctl daemon-reload
sudo systemctl enable home-assistant.service
```

2. To sart the service:

```bash
sudo systemctl start home-assistant.service
```

3. To stop the service:

```bash
sudo systemctl stop home-assistant.service
```

4. To check the status of the service:

```bash
sudo systemctl status home-assistant.service
```

## vboxmanage useful commands

1. List virtual machines

```bash
sudo vboxmanage list vms
```

2. List running virtual machines

```bash
sudo vboxmanage list runningvms
```

3. List info about some virtual machine

```bash
sudo vboxmanage showvminfo home-assistant-hassio 
```

**Note**: This VM was installed with root user because we need to attacth a hub/stick for other usability. This is only possible using the root user. Note, thought, that, running "sudo list runningvms" will list only the machines running for the sudo user and not all the machines running.

## Add a pen stick to home assistant

1. Turn off the machine

```
sudo systemctl stop home-assistant.service
```

2. turn on the usb 

```
sudo vboxmanage modifyvm "home-assistant-hassio" --usb on
```

3. Turn on the machine

```
sudo systemctl start home-assistant.service
```

4. Check the list of available USB's on the machine

```
sudo VBoxManage list usbhost
```

5. Grab the ```UUID``` from the USB that you want and attach it to vm:

```
vboxmanage controlvm "home-assistant-hassio" usbattach <UUID of USB>
```

6. Check if everrything went well on the frontend of home-assistant: Go to the "supervisor" tab, "System", and on the "Host", choose the option "hardware". You should have some entrie for your usb (A good way to find it is to search fro the SERIAL NUMBER (can be found when listing the available machines)).


7. If you want to dettach the usb from the vm:

```
vboxmanage controlvm "home-assistant-hassio" usbdetach <UUID of USB>
```

# sources

http://hadenpike.net/2017/01/24/how-to-use-the-virtualbox-command-line.html

https://www.virtualbox.org/manual/ch01.html#gui-createvm

https://www.virtualbox.org/manual/ch03.html#settings-motherboard

https://www.virtualbox.org/manual/ch08.html


