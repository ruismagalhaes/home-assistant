# Introduction

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

# Table of contents

[[_TOC_]] 


# Install espHome addon on Home-assistant

1. Go to the [home assistant link](http://homeassistant.local:8123). Something like the following should appear:

<div align="center">
<img src="guides/images/home-assistant-login.png"  width="400" height="400">
</div>

2. Log In with your credentials created when you installed HASSIO ([guide for installation available here](https://gitlab.com/ruismagalhaes/home-assistant/-/blob/master/guides/HASSIO_virtualbox_install.md)).
After logging in, go to the side bar, click on "**supervisor**", "**Add-on store**" and Reload the repositories available as shown on the image below:

<div align="center">
<img src="guides/images/home-assistant-reload-repositories.png"  width="700" height="450">
</div>

3. Search for "**espHome**" on the search bar, select and install it and select "**start on boot**", "**Auto update**" and "**Show in sidebar**". The image below show you where those choices are:

<div align="center">
<img src="guides/images/home-assistant-esphome-install.png"  width="700" height="450">
</div>

4. After that, go to the ESPHome (available on the sidebar), and add a new device. Choose a name, a device type (for shelly 1PM is the generic esp8266), connect it to the wifi. The process is shown below:

<div align="center">
<img src="guides/images/home-assistant-add-esphome-device.png"  width="700" height="450">
</div>

5. When successfully, an offline device will appear on the "**ESPHome**" page. If you click on "**edit**" you will see the following code:

```yaml

esphome:
  name: testing_plug
  platform: ESP8266
  board: esp01_1m


wifi:
  ssid: wifi_name
  password: wifi_password

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: "Testing Plug Fallback Hotspot"
    password: hotspot_password

captive_portal:

# Enable logging
logger:

# Enable Home Assistant API
api:

ota:

```

**NOTE**: This code is only for the SHELLY 1PM to get connected to the WIFI network. From here you can do OTA (Over the air) updates.

6. Now, click on "compile" and check if it was succesfully. Then, download the binary.


<div align="center">
<img src="guides/images/home-assistant-binary-download.png"  width="700" height="450">
</div>


# Flash Shelly 1PM with EspHome

In order to flash Shelly 1PM you will need the following:

 - [Shelly 1PM](https://mauser.pt/catalog/product_info.php?cPath=570_726&products_id=096-6997)
 - eshome-flasher (download from github repo on [sources](https://gitlab.com/ruismagalhaes/home-assistant/-/blob/master/guides/03-shelly_1PM_esphome.md#sources))
 - [Cables jumper Dupont macho-fêmea](https://mauser.pt/catalog/product_info.php?cPath=1874_56_2732_2787&products_id=096-7938)
 - [FTDI converter](https://mauser.pt/catalog/product_info.php?cPath=1667_2604_2607&products_id=096-6949)
 - [Cables usb-c to mini-usb](https://mauser.pt/catalog/product_info.php?cPath=1874_57_107_2473_2271&products_id=047-1908)

## Shelly Pins

- On the image below, we can see the Schema of the SHELLY 1PM pins:

<div align="center">
<img src="guides/images/shelly1pins.png"  width="400" height="200">
</div>

## FTDI converter Pins

- On the image below, we can see the Schema of the FTDI CONVERTER pins:

<div align="center">
<img src="guides/images/ftdipins.png"  width="400" height="200">
</div>

## cable connections SHELLY-FTDI schema

- In order to start the process of flashing the shelly 1PM, the **[Cables jumper Dupont ](https://mauser.pt/catalog/product_info.php?cPath=1874_56_2732_2787&products_id=096-7938)** should be connected to the **[FTDI converter](https://mauser.pt/catalog/product_info.php?cPath=1667_2604_2607&products_id=096-6949)** and to the **Shelly 1PM** as follows:

<div align="center">
<img src="guides/images/shellyftdiconnections.png"  width="450" height="150">
</div>

- As it can be seen on the image, to flash the **Shelly 1PM** you have to connect the **[Cables jumper Dupont ](https://mauser.pt/catalog/product_info.php?cPath=1874_56_2732_2787&products_id=096-7938)** to the **[FTDI converter](https://mauser.pt/catalog/product_info.php?cPath=1667_2604_2607&products_id=096-6949)** as following:


| Shelly 1 PM |                  FTDI                 |
| :------------:|:-----------------------------------:|
| GND         | GND                                  |
| GPIO0       | GND (When flashing is the objective) |
| 3.3V        | VCC                                  | 
| RXD         | TX                                   | 
| TXD         | RX                                   | 


- Now, before connecting the FTDI to the usb of the computer, make sure that everyhing is like the image above and that the GPIO0 of the shelly 1 PM is touching the ground (GND). Then, connect the FTDI to the USB and after +/- 10s you can let the GPIO0 disconnected from GND.

**NOTE1** : The [FTDI converter](https://mauser.pt/catalog/product_info.php?cPath=1667_2604_2607&products_id=096-6949) shoud be on the 3.3V (check jumper position)

- My connections:

<div align="center">
<img src="guides/images/fdticonnections.png"  width="200" height="300">
</div>

<div align="center">
<img src="guides/images/shelly1pmconnections.png"  width="200" height="200">
</div>

- Finally, open [eshome-flasher](https://gitlab.com/ruismagalhaes/home-assistant/-/blob/master/guides/shelly_1PM_esphome.md#sources) (just write $esphomeflasher on the terminal), and select the option available on the "**serial port**", choose the binary downloaded on the previous step on the "**Firmaware**" row and click "**flash ESP**". After that, follow the logs and check if everything went successfully.

**At this point, and if everything went as expected, your shelly should be flashed with ESPHome software. On top of that, it should be able to connect to your wifi network, when powered.**

# Configuring shelly 1PM

## Eletrical plug

- To be easier to identify **an eletrical plug**, here goes one example:

<div align="center">
<img src="guides/images/eletrical-plug-example.png"  width="200" height="200">
</div>

- The eletrical diagram to connect a shelly 1PM to an eletrical plug is available on the image below:

<div align="center">
<img src="guides/images/shelly1pm-eletrical-plug-diagram.png"  width="700" height="450">
</div>

- Below, an image of my eletrical connection to the shelly is available too (black is powered line, blue is neutral and the yellow is ground):

<div align="center">
<img src="guides/images/shelly1pm-eletrical-plug-connection.png"  width="700" height="450">
</div>

- After achieving what's on the images above, the device on the "**ESPHome**" page should be online. From here, you can edit the code to the following:

```yaml
substitutions:
  plug_name: testing_plug

esphome:
  name: ${plug_name}
  platform: ESP8266
  board: esp01_1m


wifi:
  ssid: wifi_network
  password: wifi_password

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: "Testing Plug Fallback Hotspot"
    password: hotspot_password

captive_portal:

# Enable logging
logger:

# Enable Home Assistant API
api:

ota:

# Relay
switch:
  - platform: gpio
    name: "${plug_name}_button"
    icon: "mdi:power-socket-eu"
    pin: GPIO15
    id: relay1
    restore_mode: ALWAYS_ON
    

sensor:
    # wifi signal for testing_plug
  - platform: wifi_signal
    name: "${plug_name} WiFi Signal"
    update_interval: 60s
    # eletricity consumption
  - platform: hlw8012
    cf_pin: GPIO05
    cf1_pin: GPIO13 # not used because it is not available on the 1PM but it is needed to compile
    sel_pin: GPIO14 # not used because it is not available on the 1PM but it is needed to compile
    power:
      name: "${plug_name} Power"
      unit_of_measurement: W
      id: "${plug_name}_power"
      icon: mdi:flash-circle
      accuracy_decimals: 0
      filters:
      # must calibrate this myself when time available
      #Reading -> actual
      - calibrate_linear:
          - 2.5 -> 0.16
          - 747.0 -> 125.0
          - 1409.0 -> 237.0
          - 2663.0 -> 444.0
          - 8600.0 -> 1390.0
      - lambda: if (x <= 6.0) return 0; else return (x - 6);
    update_interval: 10s
    
  # NTC Temperature
  # Reading temperature of shelly 1 PM
  - platform: ntc
    sensor: ${plug_name}_resistance_sensor
    name: "${plug_name} Temperature"
    unit_of_measurement: "°C"
    accuracy_decimals: 1
    icon: mdi:thermometer
    calibration:
      b_constant: 3350
      reference_resistance: 10kOhm
      reference_temperature: 298.15K
  - platform: resistance
    id: ${plug_name}_resistance_sensor
    sensor: ${plug_name}_source_sensor
    configuration: DOWNSTREAM
    resistor: 32kOhm
  - platform: adc
    id: ${plug_name}_source_sensor
    pin: A0
    update_interval: 10s

```

- After editing the code, click upload. If everything was done the right way, you should see the code compiling and uploading to the shelly 1PM. Finally, you can add some cards to your Home assistant. On the image below I have a button, an entity card, some history graphs and a glance card. 

<div align="center">
<img src="guides/images/shelly1pm-cards-example-hassio.png"  width="700" height="450">
</div>

**NOTE**:A lot more can be achieved here. However, this is not the purpose of this guide.


## One light switch with 1 gang

- To be easier to identify **one light switch with 1 gangs**, here goes one example:

<div align="center">
<img src="guides/images/1-gang-light-switch.png"  width="200" height="200">
</div>





## One light switch with 2 gangs

- To be easier to identify **one light switch with 2 gangs**, here goes one example:

<div align="center">
<img src="guides/images/2-gang-light-switch.png"  width="200" height="200">
</div>

This is not possible with SHELLY 1PM because each gang connects to one separate circuit and shelly 1PM only has 1 RELAY.
For this, you would need a SHELLY 2.5 (it has 2 channels/relays and it can monitor the power for each channel/relay).

If you want to use a SHELLY 1PM, the only way to do it is using 2 shelly's: 1 for each gang/circuit (for this you can follow [this guide](https://gitlab.com/ruismagalhaes/home-assistant/-/blob/master/guides/03-shelly_1PM_esphome.md#one-light-switch-with-1-gang))


## Two light switches for the same light bulb



# sources

https://github.com/esphome/esphome-flasher

https://community.home-assistant.io/t/shelly-1pm/118929/18

https://esphome.io/

